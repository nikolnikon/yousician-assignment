import pytest
import json

from bson import ObjectId


def test_song_list_ok(client, init_db_with_songs):
    response = client.get('/songs')
    assert response.status_code == 200
    assert {'results', 'limit', 'offset', 'total'}.issubset(response.get_json().keys())


@pytest.mark.parametrize(
    "limit, expected",
    [(2, 2), (None, 10)],
    ids=['limit_set', 'no_limit']
)
def test_song_list_limit(limit, expected, client, init_db_with_songs):
    response = client.get(f'/songs?limit={limit}') if limit is not None else client.get('/songs')
    assert len(response.get_json().get('results')) == expected


@pytest.mark.parametrize(
    "limit, offset, expected",
    [(-1, 0, 400), ('ham', 'spam', 400)],
    ids=['negative', 'string']
)
def test_song_list_params_validation(limit, offset, expected, client, init_db_with_songs):
    response = client.get(f'/songs?limit={limit}&offset={offset}')
    assert response.status_code == expected


@pytest.mark.parametrize(
    "message, expected",
    [('Vivaldi', 1), ('ViVaLdI', 1), ('trailrunning', 0)],
    ids=['match', 'case_insensitive', 'doesn\'t_match']
)
def test_search_songs(message, expected, client, init_db_with_songs):
    response = client.get(f'/songs/search?message={message}')
    assert len(response.get_json().get('results')) == expected
    if expected > 0:
        artist = response.get_json().get('results')[0].get('artist').lower()
        title = response.get_json().get('results')[0].get('title').lower()
        assert message.lower() in artist or message.lower() in title


def test_search_songs_params_validation(client, init_db_with_songs):
    response = client.get('/songs/search')
    assert response.status_code == 400


@pytest.mark.parametrize(
    "level, expected",
    [(None, 10.32), (9, 9.69)],
    ids=['no_level', 'level_9']
)
def test_average_difficulty(level, expected, client, init_db_with_songs):
    response = client.get('/songs/difficulty') if level is None else client.get(f'/songs/difficulty?level={level}')
    assert response.get_json().get('average_difficulty') == expected


def test_add_song_rating(client, mongo_client, init_db_with_songs, init_db_with_ratings):
    response = client.post(
            '/songs/60e99eb0569912613e8c1447/rating',
            data=json.dumps({'rating': 1}),
            content_type='application/json'
    )

    song = mongo_client.songs_db_test.songs.find_one({'_id': ObjectId('60e99eb0569912613e8c1447')})
    highest = song.get('max_rating')
    lowest = song.get('min_rating')
    average = song.get('average_rating')

    assert response.status_code == 201
    assert highest == 5
    assert lowest == 1
    assert average == 3.25


@pytest.mark.parametrize(
    "song_id, rating, expected",
    [
        ('60e99eb0569912613e8c1451', 3, 404),
        ('60e99eb0569912613e8c1447', 8, 400),
        ('60e99eb0569912613e8c1447', 'ham', 400),
        ('60e99eb0569912613e8c1447', None, 400)
    ],
    ids=['wrong_song_id', 'wrong_rating_range', 'wrong_rating_type', 'no_rating']
)
def test_add_song_rating_params_validation(song_id, rating, expected, client, init_db_with_songs, init_db_with_ratings):
    data = {'rating': rating} if rating is not None else None
    response = client.post(
        f'/songs/{song_id}/rating',
        data=json.dumps(data),
        content_type='application/json'
    )

    assert response.status_code == expected


def test_get_song_rating(client, init_db_with_songs, init_db_with_ratings):
    response = client.get('/songs/60e99eb0569912613e8c1447/rating')
    highest = response.get_json().get('max_rating')
    lowest = response.get_json().get('min_rating')
    average = response.get_json().get('average_rating')

    assert highest == 5
    assert lowest == 3
    assert average == 4.0
