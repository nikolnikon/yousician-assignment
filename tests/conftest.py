import pytest
import json

from pytest_mongo import factories
from bson import ObjectId

from songs import create_app


@pytest.fixture
def app():
    app = create_app({'TESTING': True, 'MONGO_URI': 'mongodb://127.0.0.1:27018/songs_db_test'})

    return app


@pytest.fixture
def client(app):
    return app.test_client()


mongo_proc = factories.mongo_noproc(port=27018)
mongo_client = factories.mongodb('mongo_proc')


@pytest.fixture
def init_db_with_songs(mongo_client):
    mongo_client.songs_db_test.songs.insert_many(
        [
            {
                "artist": "The Yousicians",
                "title": "Lycanthropic Metamorphosis",
                "difficulty": 14.6,
                "level": 13,
                "released": "2016-10-26"
            },
            {
                "artist": "The Yousicians",
                "title": "A New Kennel",
                "difficulty": 9.1,
                "level": 9,
                "released": "2010-02-03"
            },
            {
                "artist": "Mr Fastfinger",
                "title": "Awaki-Waki",
                "difficulty": 15,
                "level": 13,
                "released": "2012-05-11"
            },
            {
                "artist": "The Yousicians",
                "title": "You've Got The Power",
                "difficulty": 13.22,
                "level": 13,
                "released": "2014-12-20"
            },
            {
                "artist": "The Yousicians",
                "title": "Wishing In The Night",
                "difficulty": 10.98,
                "level": 9,
                "released": "2016-01-01"
            },
            {
                "artist": "The Yousicians",
                "title": "Opa Opa Ta Bouzoukia",
                "difficulty": 14.66,
                "level": 13,
                "released": "2013-04-27"
            },
            {
                "artist": "The Yousicians",
                "title": "Greasy Fingers - boss level",
                "difficulty": 2,
                "level": 3,
                "released": "2016-03-01"
            },
            {
                "artist": "The Yousicians",
                "title": "Alabama Sunrise",
                "difficulty": 5,
                "level": 6,
                "released": "2016-04-01"
            },
            {
                "artist": "The Yousicians",
                "title": "Can't Buy Me Skills",
                "difficulty": 9,
                "level": 9,
                "released": "2016-05-01"
            },
            {
                "artist": "The Yousicians",
                "title": "Vivaldi Allegro Mashup",
                "difficulty": 13,
                "level": 13,
                "released": "2016-06-01"
            },
            {
                "_id": ObjectId('60e99eb0569912613e8c1447'),
                "artist": "The Yousicians",
                "title": "Babysitting",
                "difficulty": 7,
                "level": 6,
                "released": "2016-07-01"
            }
        ]
    )


@pytest.fixture
def init_db_with_ratings(client):
    ratings = [
        {
            "rating": 4,
        },
        {
            "rating": 5,
        },
        {
            "rating": 3,
        }
    ]

    for rating in ratings:
        client.post(
            '/songs/60e99eb0569912613e8c1447/rating',
            data=json.dumps(rating),
            content_type='application/json'
        )
