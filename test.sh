#!/bin/zsh

# Create virtual environment
virtualenv venv --python=python3

# Install requirements
source ./venv/bin/activate
pip install -r requirements.txt

# Start and init MongoDB
docker stop songs_db_test
sleep 2
docker rm songs_db_test
sleep 2
docker run --detach --name songs_db_test --publish 127.0.0.1:27018:27017 mongo:4.4

# Run tests
pytest