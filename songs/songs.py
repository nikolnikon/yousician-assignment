import json
import datetime

from flask import Blueprint, request
from flask.json import dumps
from flask.globals import current_app
from werkzeug.exceptions import HTTPException, NotFound
from bson import json_util, ObjectId
from .validators import (
    validate_song_list_params,
    validate_search_songs_params,
    validate_difficulty_params,
    validate_add_songs_rating_params
)

from songs import mongo

bp = Blueprint('songs', __name__, url_prefix='/songs')


class OIJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


@bp.errorhandler(Exception)
def handle_error(e):
    return {
               'message': e.description if isinstance(e, HTTPException) else str(e),
           }, e.code if isinstance(e, HTTPException) else 500


@bp.route('')
@validate_song_list_params
def get_song_list():
    limit = int(request.args.get('limit', 10))
    skip = int(request.args.get('offset', 0))
    total = mongo.db.songs.count_documents({})
    songs = list(mongo.db.songs.find(limit=limit, skip=skip).sort('_id'))

    data = {
        'limit': limit,
        'offset': skip,
        'total': total,
        'results': songs,
    }

    return current_app.response_class(
        f'{dumps(data, cls=OIJSONEncoder)}\n',
        mimetype=current_app.config["JSONIFY_MIMETYPE"],
    )


@bp.route('/search')
@validate_search_songs_params
def search_songs():
    message = request.args.get('message')
    fltr = {
        '$or': [
            {'artist': {'$regex': message, '$options': 'i'}},
            {'title': {'$regex': message, '$options': 'i'}}
        ]
    }
    limit = int(request.args.get('limit', 10))
    skip = int(request.args.get('offset', 0))
    total = mongo.db.songs.count_documents(fltr)
    songs = list(mongo.db.songs.find(
        filter=fltr,
        limit=limit,
        skip=skip
    ).sort('_id'))

    data = {
        'limit': limit,
        'offset': skip,
        'total': total,
        'results': songs
    }

    return current_app.response_class(
        f'{dumps(data, cls=OIJSONEncoder)}\n',
        mimetype=current_app.config["JSONIFY_MIMETYPE"],
    )


@bp.route('/difficulty')
@validate_difficulty_params
def get_average_difficulty():
    level = int(request.args.get('level', 0))
    pipeline = [
            {
                '$group': {
                    '_id': None,
                    'difficulty': {
                        '$avg': '$difficulty'
                    }
                }
            }
        ]

    if level:
        pipeline.insert(0, {'$match': {'level': level}})

    cursor = mongo.db.songs.aggregate(pipeline)

    try:
        difficulty = cursor.next().get('difficulty')
    except StopIteration:
        difficulty = None

    return {'average_difficulty': round(difficulty, 2) if type(difficulty) is float else None}


@bp.route('/<string:song_id>/rating', methods=['POST'])
@validate_add_songs_rating_params
def add_song_rating(song_id):
    rating = int(request.json.get('rating'))
    song_id = ObjectId(song_id)

    if mongo.db.songs.count_documents({'_id': song_id}, limit=1) <= 0:
        raise NotFound(description=f'Song with id={str(song_id)} doesn\'t exist')

    with mongo.cx.start_session() as session:
        mongo.db.ratings.insert_one(
            {
                'song_id': song_id,
                'rating': rating,
                'created_at': datetime.datetime.utcnow(),
                'updated_at:': datetime.datetime.utcnow()
            }
        )

        mongo.db.ratings.aggregate(
            [
                {
                    '$match': {
                        'song_id': song_id
                    }
                },
                {
                    '$group': {
                        '_id': '$song_id',
                        'average_rating': {
                            '$avg': '$rating'
                        },
                        'max_rating': {
                            '$max': '$rating'
                        },
                        'min_rating': {
                            '$min': '$rating'
                        }
                    }
                },
                {
                    '$set': {
                        '_id': song_id
                    }
                },
                {
                    '$merge': {
                        'into': 'songs',
                        'on': '_id',
                        'whenMatched': 'merge',
                        'whenNotMatched': 'discard'
                    }
                }
            ]
        )

    return {'message': 'Rating was successfully added'}, 201


@bp.route('/<string:song_id>/rating', methods=['GET'])
def get_song_rating(song_id):
    song_id = ObjectId(song_id)

    if mongo.db.songs.count_documents({'_id': song_id}, limit=1) <= 0:
        raise NotFound(description=f'Song with id={str(song_id)} doesn\'t exist')

    song = mongo.db.songs.find_one({'_id': song_id}, {'difficulty': 0, 'level': 0, 'released': 0})

    if not {'average_rating', 'max_rating', 'min_rating'}.issubset(song.keys()):
        song.update(
            {
                'average_rating': None,
                'max_rating': None,
                'min_rating': None
            }
        )
    else:
        song['average_rating'] = round(song['average_rating'], 2)

    return current_app.response_class(
        f'{dumps(song, cls=OIJSONEncoder)}\n',
        mimetype=current_app.config["JSONIFY_MIMETYPE"],
    )
