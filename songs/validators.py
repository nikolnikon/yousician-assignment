import functools
from werkzeug.exceptions import BadRequest
from flask import request


def _validate_limit_offset():
    try:
        limit = int(request.args.get('limit', 10))
        skip = int(request.args.get('offset', 0))
    except ValueError:
        raise BadRequest(description='Wrong type of limit/offset. Should be integer')

    if limit <= 0 or skip < 0:
        raise BadRequest(description='\'limit\'/\'offset\' should be above zero')


def validate_song_list_params(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        # try:
        #     limit = int(request.args.get('limit', 10))
        #     skip = int(request.args.get('offset', 0))
        # except ValueError:
        #     raise BadRequest(description='Wrong type of limit/offset. Should be integer')
        #
        # if limit <= 0 or skip < 0:
        #     raise BadRequest(description='\'limit\'/\'offset\' should be above zero')
        _validate_limit_offset()

        return f(*args, **kwargs)
    return decorated_function


def validate_search_songs_params(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        _validate_limit_offset()
        message = request.args.get('message')
        if not message:
            raise BadRequest(description='Query string should contain \'message\' parameter')

        return f(*args, **kwargs)
    return decorated_function


def validate_difficulty_params(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        try:
            level = int(request.args.get('level', 0))
        except ValueError:
            raise BadRequest(description='Wrong type of \'level\'. Must be integer')

        return f(*args, **kwargs)
    return decorated_function


def validate_add_songs_rating_params(f):
    @functools.wraps(f)
    def decorated_function(*args, **kwargs):
        if not request.json:
            raise BadRequest(description='Request should have body')

        rating = request.json.get('rating')
        if not rating:
            raise BadRequest(description='Request body should contain \'rating\'')

        try:
            rating = int(rating)
        except ValueError:
            raise BadRequest(description='Wrong type of \'rating\'. Should be integer')

        if rating not in range(1, 6):
            raise BadRequest(description='\'rating\' should be in range [1, 5]')

        return f(*args, **kwargs)
    return decorated_function




