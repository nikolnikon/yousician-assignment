import os

from flask import Flask
from flask_pymongo import PyMongo

mongo = PyMongo()


def create_app(test_config=None):
    app = Flask(__name__)
    if test_config is None:
        app.config['MONGO_URI'] = os.getenv('MONGO_URI', 'mongodb://127.0.0.1:27017/songs_db')
    else:
        app.config.update(test_config)

    mongo.init_app(app)

    from . import songs
    app.register_blueprint(songs.bp)

    return app
