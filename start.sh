#!/bin/zsh

# Create virtual environment
virtualenv venv --python=python3

# Install requirements
source ./venv/bin/activate
pip install -r requirements.txt

# Start and init MongoDB
docker stop songs_db
sleep 2
docker rm songs_db
sleep 2
docker run --detach --name songs_db --publish 127.0.0.1:27017:27017 mongo:4.4
sleep 2
docker exec -it songs_db mongo songs_db --eval "db.songs.insert([{\"artist\": \"The Yousicians\",\"title\": \"Lycanthropic Metamorphosis\",\"difficulty\": 14.6,\"level\":13,\"released\": \"2016-10-26\"}, {\"artist\": \"The Yousicians\",\"title\": \"A New Kennel\",\"difficulty\": 9.1,\"level\":9,\"released\": \"2010-02-03\"}, {\"artist\": \"Mr Fastfinger\",\"title\": \"Awaki-Waki\",\"difficulty\": 15,\"level\":13,\"released\": \"2012-05-11\"}, {\"artist\": \"The Yousicians\",\"title\": \"You've Got The Power\",\"difficulty\": 13.22,\"level\":13,\"released\": \"2014-12-20\"}, {\"artist\": \"The Yousicians\",\"title\": \"Wishing In The Night\",\"difficulty\": 10.98,\"level\":9,\"released\": \"2016-01-01\"}, {\"artist\": \"The Yousicians\",\"title\": \"Opa Opa Ta Bouzoukia\",\"difficulty\": 14.66,\"level\":13,\"released\": \"2013-04-27\"}, {\"artist\": \"The Yousicians\",\"title\": \"Greasy Fingers - boss level\",\"difficulty\": 2,\"level\":3,\"released\": \"2016-03-01\"}, {\"artist\": \"The Yousicians\",\"title\": \"Alabama Sunrise\",\"difficulty\": 5,\"level\":6,\"released\": \"2016-04-01\"}, {\"artist\": \"The Yousicians\",\"title\": \"Can't Buy Me Skills\",\"difficulty\": 9,\"level\":9,\"released\": \"2016-05-01\"}, {\"artist\": \"The Yousicians\",\"title\": \"Vivaldi Allegro Mashup\",\"difficulty\": 13,\"level\":13,\"released\": \"2016-06-01\"}, {\"artist\": \"The Yousicians\",\"title\": \"Babysitting\",\"difficulty\": 7,\"level\":6,\"released\": \"2016-07-01\"}])"

# Run the service
export MONGO_URI=mongodb://127.0.0.1:27017/songs_db
export FLASK_APP=songs
flask run
